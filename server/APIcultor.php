<?php
#
# APIcultor - API basics
# (c) 2020 Frederic Pasteleurs <frederic@askarel.be>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program or from the site that you downloaded it
# from; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307   USA
#

# Populate chosen template
# $templatefile is the name of a text file containing the text body
# $templatedata is an array containing the variable(s) to substitute
# Returns to caller with the template loaded and the placeholders substituted by data
# Placeholder are in the form {{ placeholder }}

function populatetemplate($templatefile, $templatedata) {
    $template = file_get_contents($templatefile);
    foreach($templatedata as $key => $values) {
	$template = str_replace('{{ ' . $key . ' }}', $values, $template);
    }
    return $template;
}

// Generate a random string. Parameter gives the length of the random string. Defaults to 20 chars
function randomstring($length=20) {
    $STR='';
    $CHRS='()*+-./0123456789:;=@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_abcdefghijklmnopqrstuvwxyz{|}';
    for ($i=0; $i < $length; $i++) {
	$STR .= $CHRS[ mt_rand(0, strlen($CHRS)-1) ];
    }
    return $STR;
}

// Taken from https://blog.michael.kuron-germany.de/2012/07/hashing-and-verifying-ldap-passwords-in-php/
function hash_password($password) { // SSHA with random 4-character salt
    $salt = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',4)),0,4);
    return '{SSHA}' . base64_encode(sha1( $password.$salt, TRUE ). $salt);
}

// Dump specified array as JSON. Set extra headers. HTTP code is optional and defaults to 200 OK
function output_array_as_json($payload, $http_code = 200) {
    http_response_code($http_code);
    header('Content-Type: application/json');
    header('Access-Control-Allow-Origin: *');
    printf ("%s\n", json_encode($payload) );
    if ( ($http_code >= 100) and ($http_code < 400) ) { // Anything between 100 and 399 are supposed to be normal responses
	exit(0);
    } else {
	exit(1);
    }
}

// authenticate an LDAP user using basic method. Returns an LDAP handle authenticated as user if successful. Bombs out on failure.
function SimpleHttpAuthLdapUser () {
    if (isset ($GLOBALS['GLOBALCONFIG']['CORE']['LDAP']['basedn'])) { // Got LDAP ?
	if (empty ($_SERVER['PHP_AUTH_USER']) and empty ($_SERVER['PHP_AUTH_PW'])) { // Username/password specified ?
	    header('WWW-Authenticate: Basic realm="' . $GLOBALS['GLOBALCONFIG']['CORE']['orgname'] . '"');
	    output_array_as_json(array ('error' => 'Authentication failed', 'message' => 'Please provide username and password'), 401 );
	} else { // Credentails provided. Check them!
	    // Construct the user DN ( uid=<username>, ou=<users OU>, <base DN (dc=example, dc=invalid)> ) and escape user input
	    $userDN = 'uid=' . ldap_escape ($_SERVER['PHP_AUTH_USER'], null, LDAP_ESCAPE_DN) . ',ou=' . $GLOBALS['GLOBALCONFIG']['CORE']['LDAP']['userou'] . ',' . $GLOBALS['GLOBALCONFIG']['CORE']['LDAP']['basedn'];
	    $userldapconn = ldap_connect('ldap://' . $GLOBALS['RUNTIME']['CORE']['LDAP']['host'], $GLOBALS['RUNTIME']['CORE']['LDAP']['port']) or output_array_as_json(array ('error' => 'LDAP failed', 'message' => ldap_error($userldapconn)), 500 );
	    ldap_set_option($userldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
	    $ldapbind = ldap_bind($userldapconn, $userDN, $_SERVER['PHP_AUTH_PW']);
	    if (!$ldapbind) { // Authentication failed. Bomb out.
		output_array_as_json(array ('error' => 'Authentication failed', 'message' => 'Wrong username or password'), 401 );
		return null;
	    } else { // Authentication successful, return handle
		return $userldapconn;
	    }
	}
    } else { // No LDAP in config. Bomb out.
	output_array_as_json (array ('error' => 'No LDAP configured' , 'message' => 'Cannot authenticate: no LDAP server configured'), 500);
    }
}



// authenticate an LDAP user using digest method. Returns an LDAP handle authenticated as user if successful. Bombs out on failure.
// To be developped, not working at the moment
function DigestHttpAuthLdapUser () {
    if (isset ($GLOBALS['GLOBALCONFIG']['CORE']['LDAP']['basedn'])) { // Got LDAP ?
	if (empty ($_SERVER['PHP_AUTH_DIGEST']) ) {
	    $realm='test realm';
	    header('WWW-Authenticate: Digest realm="' . $realm . '",qop="auth",nonce="'.uniqid().'",opaque="'.md5($realm).'"');
	    output_array_as_json(array ('error' => 'Authentication failed', 'message' => 'Please provide username and password'), 401 );
	} else {

exit(0);
	    // Construct the user DN ( uid=<username>, ou=<users OU>, <base DN (dc=example, dc=invalid)> ) and escape user input
	    $userDN = 'uid=' . ldap_escape ($_SERVER['PHP_AUTH_USER'], null, LDAP_ESCAPE_DN) . ',ou=' . $GLOBALS['GLOBALCONFIG']['CORE']['LDAP']['userou'] . ',' . $GLOBALS['GLOBALCONFIG']['CORE']['LDAP']['basedn'];
	    $userldapconn = ldap_connect('ldap://' . $GLOBALS['RUNTIME']['CORE']['LDAP']['host'], $GLOBALS['RUNTIME']['CORE']['LDAP']['port']) or output_array_as_json(array ('error' => 'LDAP failed', 'message' => ldap_error($userldapconn)), 500 );
	    ldap_set_option($userldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
	    $ldapbind = ldap_bind($userldapconn, $userDN, $_SERVER['PHP_AUTH_PW']);
	    if (!$ldapbind) {
		output_array_as_json(array ('error' => 'Authentication failed', 'message' => 'Wrong username or password'), 401 );
	    }
	}
    } else {
	output_array_as_json (array ('error' => 'No LDAP configured' , 'message' => 'Cannot authenticate: no LDAP server configured'), 500);
    }
}

// Registers a redirect from a legacy URL to a new style URL. Fix end '/' if needed
function register_legacy_path ($legacyPath, $redirectPath) {
    if (! (substr ($legacyPath, -1) === '/')) {
	$FixedLegacyPath = $legacyPath . '/';
    }
    if (isset ($GLOBALS['legacyPaths'][$FixedLegacyPath])) {
	output_array_as_json (array ('error' => 'Legacy path already defined' , 'message' => 'Legacy enpoint defined earlier in file ' . $GLOBALS['legacyPaths'][$FixedLegacyPath]['definedInFile'] . ' line ' . $GLOBALS['legacyPaths'][$FixedLegacyPath]['definedInLine'] .'. This is a bug.'), 500);
    } else {
	$GLOBALS['legacyPaths'][$FixedLegacyPath]['definedInFile']= debug_backtrace()[0]['file'];
	$GLOBALS['legacyPaths'][$FixedLegacyPath]['definedInLine']= debug_backtrace()[0]['line'];
	if (substr ($redirectPath, -1) === '/') { 
	    $GLOBALS['legacyPaths'][$FixedLegacyPath]['NewPath']=$redirectPath;
	} else {
	    $GLOBALS['legacyPaths'][$FixedLegacyPath]['NewPath']=$redirectPath . '/';
	}
    }
}

# Register an API function. Bombs out if endpoint is already defined or if function to call does not exists.
function register_api_call ($method, $pathStart, $callback, $auth=true, $desc = '') {
    if (! (substr ($pathStart, -1) === '/')) { // Path fixup: add '/' if it does not end with it.
	$pathStart = $pathStart . '/';
    }
    $method=strtoupper($method); // Make sure the request verb is uppercase in the array
    if (isset ($GLOBALS['ApiCallTable'][$pathStart][$method])) {
	output_array_as_json (array ('error' => 'Endpoint already defined' , 'message' => 'API enpoint defined earlier in file ' . $GLOBALS['ApiCallTable'][$pathStart][$method]['definedInFile'] . ' line ' . $GLOBALS['ApiCallTable'][$pathStart][$method]['definedInLine'] .'. This is a bug.'), 500);
    } else {
	$GLOBALS['ApiCallTable'][$pathStart][$method]['definedInFile']= debug_backtrace()[0]['file']; // Fill in some debug data
	$GLOBALS['ApiCallTable'][$pathStart][$method]['definedInLine']= debug_backtrace()[0]['line'];
	if (function_exists($callback)) {
	    $GLOBALS['ApiCallTable'][$pathStart][$method]['auth']= $auth; // Authentication type requested
	    $GLOBALS['ApiCallTable'][$pathStart][$method]['callback']= $callback; // Callback function
	    $GLOBALS['ApiCallTable'][$pathStart][$method]['description']= $desc; // Optional description
	} else {
	    output_array_as_json (array ('error' => 'Function does not exist' , 'message' => 'Function ' . $callback . ' does not exist. This is a bug.'), 500);
	}
    }
}

# Process the legacy redirects
function legacy_path_redirect ($apiPath) {
    foreach ($GLOBALS['legacyPaths'] as $legacyKey => $legacyPath) {
	$subPath=substr($apiPath, 0, strlen ($legacyKey));
	if ($subPath === $legacyKey) {
	    $redirectPath=str_replace ($legacyKey, $GLOBALS['legacyPaths'][$legacyKey]['NewPath'], $apiPath);
	    header ("Location: ". $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'] . $_SERVER['SCRIPT_NAME'] . $redirectPath, true, 308);
	    exit(0);
	}
    }
}

# Execute the call with parameters
function execute_api_call ($method, $apiPath) {
    foreach ($GLOBALS['ApiCallTable'] as $ApiCallKey => $ApiCallValue) {
	$subPath=substr($apiPath, 0, strlen ($ApiCallKey));
//	printf ("--\n");var_dump ($apiPath, $ApiCallKey, $subPath); printf ("--\n");
	if ($subPath === $ApiCallKey) { // We have a valid path !
	    $endPathArr=explode ('/', substr($apiPath, strlen ($ApiCallKey))); // Convert the rest of the string to an array of parameters
	    if (isset ($ApiCallValue[strtoupper($method)])) {
		switch (strtoupper($ApiCallValue[strtoupper($method)]['auth'])) {
		    case 'SIMPLEHTTP':
			break;
		    case null:
			call_user_func_array($ApiCallValue[strtoupper($method)]['callback'], $endPathArr);
			break;
		    default:
			output_array_as_json(array ('error' => 'Bad authentication method requested for that call'), 500);
		}
	    } else {
		output_array_as_json(array ('error' => 'Method not allowed'), 405);
	    }
	}
    }
    output_array_as_json(array ('error' => 'Endpoint not found'), 404);
}


//			printf ("------------------------------------------\n");
//			var_dump ($apiPath, $ApiCallKey, $ApiCallValue, $subPath);
//			output_array_as_json(array ('message' => 'Endpoint found!'));
//	    printf ("Legacy path: %s redirect path: %s apiPath: %s, resultPath: %s\n", $legacyKey, $GLOBALS['legacyPaths'][$legacyKey]['NewPath'], $apiPath, $redirectPath );
//	    printf ("strncmp (%s, %s, strlen (legacyKey)) = %s\n", $apiPath, $legacyKey, strncmp ($apiPath, $legacyKey, strlen ($legacyKey)));
//	    printf ("++++++++++++++++++++++++++++++++++++++++++++++++\n");
//	    var_dump ($GLOBALS['ApiCallTable']);

##############################################################################################################################"

# Config file location
$MYLOCATION=dirname (__FILE__);
$GLOBALCONFIGFILE=$MYLOCATION . '/globalconfig.php';
# Does the config file exist ? Bomb out if it does not.
if (!file_exists($GLOBALCONFIGFILE)) {
    output_array_as_json (array ('error' => 'File ' . $GLOBALCONFIGFILE . ' does not exist.', 'message' => 'Please run the setup script to create config file.'), 500);
}

include_once($GLOBALCONFIGFILE);

// Set some Runtime defaults in case they are undefined in the config. Works most of the time
//$RUNTIME['CORE']['sessioncookieduration'] =@$GLOBALCONFIG['CORE']['sessioncookieduration'] ?: 7200;
$RUNTIME['CORE']['SQLDB']['host'] = @$GLOBALCONFIG['CORE']['SQLDB']['host'] ?:'localhost';
$RUNTIME['CORE']['LDAP']['host'] = @$GLOBALCONFIG['CORE']['LDAP']['host'] ?:'localhost';
$RUNTIME['CORE']['LDAP']['port'] = @$GLOBALCONFIG['CORE']['LDAP']['port'] ?:389;
$RUNTIME['CORE']['ApiPathDepth'] = @$GLOBALCONFIG['CORE']['ApiPathDepth'] ?:20;

// We have the config file, start the session (REST APIs should be stateless)
//session_start([ 'cookie_lifetime' => $RUNTIME['CORE']['sessioncookieduration'] ]); // Two hours cookie

# Open a database connection if specified in config
try {
    if (isset ($GLOBALCONFIG['CORE']['SQLDB']['database'])) {
	$dsn = "mysql:host=". $RUNTIME['CORE']['SQLDB']['host'] . ";dbname=" . $GLOBALCONFIG['CORE']['SQLDB']['database'] . ";charset=UTF8";
//	$sqlconn = new PDO ($dsn , $GLOBALCONFIG['CORE']['SQLDB']['user'], $GLOBALCONFIG['CORE']['SQLDB']['password']);
//	$sqlconn->setAttribute (PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//	$sqlconn->setAttribute (PDO::ATTR_EMULATE_PREPARES, false);
    }
}
catch (PDOException $e) {
    output_array_as_json(array ('error' => 'Database failed', 'message' => $e->GetMessage()), 500 );
}

# Open a LDAP connection if specified in config
try {
    if (isset ($GLOBALCONFIG['CORE']['LDAP']['basedn'])) {
	$appldapconn = ldap_connect('ldap://' . $RUNTIME['CORE']['LDAP']['host'], $RUNTIME['CORE']['LDAP']['port']);
	ldap_set_option($appldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
//	if (isset($GLOBALCONFIG['CORE']['LDAP']['appbinddn']) and (isset($GLOBALCONFIG['CORE']['LDAP']['appbindpw'])) {
	    $ldapbind = ldap_bind($appldapconn, $GLOBALCONFIG['CORE']['LDAP']['appbinddn'], $GLOBALCONFIG['CORE']['LDAP']['appbindpw']);
	    if (!$ldapbind) {
		throw new Exception(ldap_error($appldapconn));
	    }
//	}
    }
}
catch (Exception $e) {
    output_array_as_json(array ('error' => 'LDAP failed', 'message' => $e->GetMessage()), 500 );
}

// Parse API endpoint (WARNING: user-input is being processed here)
if (isset ($_SERVER['PATH_INFO'])) $ApiRequestStr=$_SERVER['PATH_INFO']; // index.php/api/blah
if (isset ($GLOBALS['_GET']['endpoint'])) $ApiRequestStr=$GLOBALS['_GET']['endpoint']; // index.php?endpoint=/api/blah
// Make sure the API call ends with a /
if ( ! (substr ($ApiRequestStr, -1) === '/')) { 
	    $ApiRequestStr = $ApiRequestStr . '/';
}



# Handles we have so far:
# - $appsqlconn - Open connection to MySQL database, as specified user and on specified database
# - $appldapconn - Open connection to LDAP server, bound (authenticated) to whatever is specified in config. Bound as 'anonymous' if no credentials provided.
# Variables defined and ready to use:
# - $GLOBALCONFIG array, with all configuration options from file
# - $ApiRequest array - all path elements exploded into individual words (to delete)
# - $ApiRequestStr - the full query string
# - $RUNTIME array - add the defaults not defined in $GLOBALCONFIG
#
# Now, run the main script...

?>