<?php
# 	REST API for HSBXL
#	(c) 2021 Frederic Pasteleurs <frederic@askarel.be>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

// Bring in the library. Config is brought by the library.
include_once('./APIcultor.php');

## Process the legacy shit. Will be deleted at some point in the future.
register_legacy_path ('/api/spaceapi', '/0.1/spaceapi');
register_legacy_path ('/api/spaceapi-old', '/0.1/spaceapi');
legacy_path_redirect ($ApiRequestStr);


// Check API call version. Load all files for that version
$CalledApiVersion=explode ('/',$ApiRequestStr, $RUNTIME['CORE']['ApiPathDepth'])[1]; // Second element (1) of the API endpoint is always the version. First element (0) is always null.
$VersionsDir=array_diff (scandir(dirname (__FILE__) . '/ver/'), array('.', '..')); // Retrieve list of available versions. Remove . and .. from listing
$VersionIndex=array_search ($CalledApiVersion , $VersionsDir); // Don't trust user input: match it against a list of valid versions and just retrieve an index
if ($VersionIndex) { // Version found! Load all API modules for that version
    foreach (array_diff (scandir(dirname (__FILE__) . '/ver/' . $VersionsDir[$VersionIndex] . '/'), array('.', '..')) as $FileToLoad) {
	if (is_file (dirname (__FILE__) . '/ver/' . $VersionsDir[$VersionIndex] . '/'. $FileToLoad)) { // We only want the files
	    require_once(dirname (__FILE__) . '/ver/' . $VersionsDir[$VersionIndex] . '/'. $FileToLoad);
	}
    }
    // Tweak the API path: trim the version away (the fileset for requested version is already loaded at this point)
    $ApiRequestStr=str_replace('/' . $VersionsDir[$VersionIndex], '', $ApiRequestStr);
} else {
    output_array_as_json(array ('error' => 'Version not found'), 404);
}
//var_dump ($GLOBALS['ApiCallTable']);
# Actually trigget the API call
execute_api_call ($_SERVER['REQUEST_METHOD'], $ApiRequestStr);

?>