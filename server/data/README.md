products.json is the price list for the fridge. 
All price/data modifications should be done inside that file. 
Just make sure the UUID at the beginning is unique.

The API is configured to fetch data straight from the RAW products.json from this git repository.
The API will convert the JSON into the old style CSV expected by Zoppas

If you want to remove a product from the displayed list, do not delete the entry: just set the attribute legacyDisplay to false

Clone, edit, commit, and restart zoppas to load latest price list!
