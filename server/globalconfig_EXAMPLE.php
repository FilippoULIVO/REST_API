<?php
# Config file should be automagically generated
# GLOBALCONFIG is a nested array. Each first level of the array MUST be the plugin name.

static $GLOBALCONFIG = array (
    'CORE' => array (
	// misc. parameters
	'lang_default' => 'EN',
	'mailfrom' => 'noreply@hackerspace.invalid',
	// Organization name
	'orgname' => 'Hackerspace Invalid',
	// Random string to encrypt the password cache
	'cachekey' => 'RaahRaahBlaah!!',
	// Length of the session cookie (will it ever be used ?)
	'sessioncookieduration' => 7200,
	// LDAP parameters for web application
	'LDAP' => array (
	    'host' => 'ldap.hackerspace.invalid',
	    'port' => 389,
	    'basedn' => 'dc=hackerspace,dc=invalid',
	    // OU where user accounts are stored
	    'userou' => 'users',
	    // Bind credentials for application. Users bind with their own DN
	    'appbinddn' => 'uid=webconcierge,ou=services,dc=hackerspace,dc=invalid',
	    'appbindpw' => 'LDAP password 389'
	    ),
	// SQL parameters for web application
	'SQLDB' => array (
	    'host' => 'database.hackerspace.invalid',
	    'port' => 3306,
	    'database' => 'exampleDB',
	    'user' => 'APIuser',
	    'password' => 'database API password'
	    )
	),
    'SPACEAPI' => array (
	'logo' => 'https://pix.hackerspace.invalid/logo.png',
	'url' => 'https://hackerspace.invalid',
	'spacefed' => array (
	    'spacenet' => false,
	    'spacesaml' => false,
	    'spacephone' => false
	    ),
	'location' => array (
	    'address' => 'Hackerstraat 42, 1337 Bruxelles, BELGIUM',
	    'lat' => 0.0,
	    'lon' => 0.0
	    ),
	'contact' => array (
	    'phone' => '+32666123456',
	    'twitter' => '@yourhackerspace',
	    'email' => 'contact@hackerspace.invalid',
	    ),
	'issue_report_channels' => array (
	    'email'
	    )
	),
    'PAMELA' => array ( // Should go to an sql table
	'pamkeys' => array ('PifPafPouf', 'PingPong'),
	),

    'PRODUCTS' => array (
	'PRODUCTFILE' => $GLOBALS['MYLOCATION'] . '/data/products.json';
	),


# Anything below that line is fluid and subject to change without notice
#####################################################################################

// LDAP fields to show to user and their properties
// r - readonly
// w - writable
// R - Required
	'LDAPUserFields' => array (
	    'uid' => 'rR',
	    'givenName' => 'w',
	    'mail' => 'wR',
	    'cn' => 'w',
	    'sn' => 'w',
	    'preferredLanguage' => 'w',
	    'description' => 'wR',
	    'homePhone' => 'w'
	    ),

// Built-in LDAP viewer
	'LDAPviewer' => array (
	    'Padlock codes' => array ( // First entry is the one selected by default.
		'readgroup' => array ( 'members', 'board'),
		'writegroup' => array ( 'board'),
		'subtree' => 'ou=Padlocks',
		'Lock' => 'x-hsbxl-padlockdescription',
		'code' => 'x-hsbxl-padlockCode'
		),
	    'Shared Passwords' => array (
		'readgroup' => array ( 'noc', 'board'),
		'writegroup' => array ( 'board'),
		'subtree' => 'ou=Password Manager',
		'Provider' => 'x-hsbxl-providerName',
		'User name' => 'x-hsbxl-providerUserName',
		'Password' => 'x-hsbxl-providerPassword'
		)
	),

// Zoppas data (will it ever be used ?)
    'ZOPPAS' => array (
	'products' => array (
		1, 
		2
	    ),
	'barcodes' => array (
	    )
	)
);

?>
